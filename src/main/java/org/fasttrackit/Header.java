package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Header {
    private final SelenideElement loginButton = $(".navbar .fa-sign-in-alt");
    private final SelenideElement greetingElement = $(".navbar-text span span");
    private final SelenideElement wishlistButton = $(".navbar .fa-heart");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private final SelenideElement homePageButton = $("[data-icon=shopping-bag]");

    public void cliclOnLoginButton() {
        loginButton.click();
        System.out.println("Click on the Login button");
    }

    public String getGreetingsMessage() {
        return greetingElement.text();
    }

    public void clickOnTheWishListIcon() {
        System.out.println("Click on the WishList button.");
        wishlistButton.click();
    }

    public void clickOnTheShoppingBagIcon() {
        System.out.println("Click on the Shopping bag icon.");
        homePageButton.click();
    }

    public void clickOnTheCartIcon() {
        System.out.println("Click on the Cart icon.");
        cartIcon.click();
    }
}
